package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Wdc044Application {
	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "world") String name){
		return String.format("Hello %s", name);
	}
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "User") String user){
		return String.format("Hi %s", user);
	}
	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name", defaultValue = "User")String name, @RequestParam(value = "age", defaultValue = "18") String age){
		return String.format("Hello " + name + "! Your age is " + age + ".");
	}
}
